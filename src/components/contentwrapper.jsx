import React from "react";
import PropTypes from "prop-types";
import TinhDiem from "../core/TinhDiem/";
import NarBar from "./narbar";
import { Route, Switch } from "react-router-dom";
import Tables from "../core/Tables";
const ContentWrapper = (props) => {
  return (
    <div id="content-wrapper" className="d-flex flex-column">
      {/* <!-- Main Content --> */}
      <div id="content">
        {/* <!-- Topbar --> */}
        <NarBar />
        {/* <!-- End of Topbar --> */}

        {/* <!-- Begin Page Content --> */}
        <Switch>
          <Route path="/dashboard" exact>
            <Tables />
          </Route>
          <Route path="/" exact>
            <TinhDiem />
          </Route>
        </Switch>
        {/* <!-- /.container-fluid --> */}
      </div>
      {/* <!-- End of Main Content --> */}

      {/* <!-- Footer --> */}
      <footer className="sticky-footer bg-white">
        <div className="container my-auto">
          <div className="copyright text-center my-auto">
            <span>Copyright &copy; F5Second</span>
          </div>
        </div>
      </footer>
    </div>
  );
};

ContentWrapper.propTypes = {};

export default ContentWrapper;
