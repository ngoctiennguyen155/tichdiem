import React from "react";
import PropTypes from "prop-types";
import { useHistory } from "react-router-dom";
import logoF5S from "../../public/img/logo-F5S.png";
const SideBar = (props) => {
  const history = useHistory();
  return (
    <ul className="navbar-nav sidebar accordion" id="accordionSidebar">
      {/* <!-- Sidebar - Brand --> */}
      <a
        className="sidebar-brand d-flex align-items-center justify-content-center"
        href="dashboard.html"
      >
        <div className="img-logo-brand">
          <img src={logoF5S} />
        </div>
        <div className="mx-3 img-logo-brand-mini">
          <img src="/public/img/logo-F5S_mini.png" />
        </div>
      </a>

      {/* <!-- Divider --> */}
      <hr className="sidebar-divider my-0" />
      {/* <!-- Menu --> */}
      <div id="menu">
        <div className="nav-item menu-item">
          <button
            className="btn nav-item"
            type="button"
            data-toggle="collapse"
            data-target="#System"
            aria-expanded="false"
            aria-controls="System"
          >
            Hệ thống
          </button>

          <div className="sub-menu-box show collapse" id="System">
            <ul className="sub-menu">
              <li className="nav-item">
                <a
                  className="nav-link"
                  onClick={() => {
                    history.push("/dashboard");
                  }}
                >
                  <i className="fas fa-list-ol"></i>
                  <span>Danh sách sản phẩm</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
        {/* <!-- Divider --> */}
        <hr className="sidebar-divider mb-0" />
        <div className="nav-item menu-item">
          <button
            className="btn nav-item"
            type="button"
            data-toggle="collapse"
            data-target="#Customer"
            aria-expanded="false"
            aria-controls="Customer"
          >
            Khách hàng
          </button>

          <div className="sub-menu-box show collapse" id="Customer">
            <ul className="sub-menu">
              <li className="nav-item">
                <a
                  className="nav-link"
                  onClick={() => {
                    history.push("/");
                  }}
                >
                  <i className="fas fa-calculator"></i>
                  <span>Tính điểm cho khách hàng</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      {/* <!-- Sidebar Toggler (Sidebar) --> */}
      <div className="text-center d-none d-md-inline">
        <button className="rounded-circle border-0" id="sidebarToggle"></button>
      </div>
    </ul>
  );
};

SideBar.propTypes = {};

export default SideBar;
