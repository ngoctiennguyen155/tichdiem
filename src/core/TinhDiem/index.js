import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import TinhDiemComponent from "./components/TinhDiem";
import channelApi from "../../client/api/channel";
import categoryApi from "../../client/api/catalogy";
import productApi from "../../client/api/product";
import userApi from "../../client/api/userApi";

const TinhDiem = (props) => {
  const [channels, setChannels] = useState([]);
  const [categories, setCategories] = useState([]);
  const [users, setUsers] = useState([]);
  const [products, setProducts] = useState([
    { id: "1", name: "SPTG A" },
    { id: "2", name: "SPTG B" },
    { id: "3", name: "SPTG C" },
    { id: "4", name: "SPTG D" },
  ]);
  useEffect(() => {
    (async () => {
      try {
        // fetch channels
        const { data: channelsData, status: statusChannels } =
          await channelApi.getAll();
        const formatDataChannels = channelsData.data.map((channel) => {
          return {
            id: channel._id,
            name: channel.name,
            tyLeDoiDiem: channel.tyLeDoiDiem,
          };
        });
        setChannels(formatDataChannels);
        // fetch categories
        const { data: categoriesData, status: statusCategories } =
          await categoryApi.getAll();
        const formatCategories = categoriesData.data.map((category) => {
          return {
            id: category._id,
            name: category.name,
            tyLeDoiDiem: category.tyLeDoiDiem,
            tyLeTrichChoKhachHang: category.tyLeTrichChoKhachHang,
          };
        });
        setCategories(formatCategories);
        // fetch products
        const { data: productsData, status: statusProducts } =
          await productApi.getAll();
        const formatData = productsData.data.map((e) => {
          return Object.assign({}, { id: e._id }, { ...e });
        });
        setProducts(formatData);
        // fetch user
        const { data: userData, status: statusUserApi } = await userApi.getAll(
          {}
        );
        const formatUserData = userData.data.map((e) => {
          return Object.assign({}, { id: e._id }, { ...e });
        });
        setUsers(formatUserData);
      } catch (error) {}
    })();
  }, []);
  return (
    <TinhDiemComponent
      channels={channels}
      products={products}
      categories={categories}
      users={users}
      categoryOnChange={async (category) => {
        const { data, status } = await productApi.getAll({
          categoryId: category,
        });
        const formatData = data.data.map((e) => {
          return Object.assign({}, { id: e._id }, { ...e });
        });
        return formatData;
      }}
    />
  );
};

TinhDiem.propTypes = {};

export default TinhDiem;
