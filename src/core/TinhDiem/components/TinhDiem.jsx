import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import historyApi from "../../../client/api/historyApi";
import uploadFileApi from "../../../client/api/uploadFileApi";
const TinhDiem = (props) => {
  const { channels, categories, products, categoryOnChange, users } = props;
  const [bangTinh, setBangTinh] = useState();
  const [listProduct, setListProduct] = useState([]);
  const [point, setPoint] = useState(0);
  const [selectLaiSuat, setSelectLaiSuat] = useState("1");
  const [file, setFile] = useState({ fileSelected: null, loading: false });
  const [summary, setSummary] = useState([]);
  useEffect(() => {
    setListProduct(
      products.filter((product) => product.categoryId === categories[0]?.id)
    );
    setBangTinh({
      indexChannel: channels.length > 0 ? 0 : -1,
      indexCategory: categories.length > 0 ? 0 : -1,
      indexProduct: products.length > 0 ? 0 : -1,
      indexUser: users.length > 0 ? 0 : -1,
    });
    (async () => {
      const response = await historyApi.getAll({
        user: users[0]?.id,
      });
      console.log(response.data.data);
      setSummary(response.data.data);
    })();
  }, [props]);
  const TinhDiem = async () => {
    const result =
      ((((bangTinh?.soDu - 0) *
        (listProduct[bangTinh?.indexProduct]?.kyHan - 0) *
        (listProduct[bangTinh?.indexProduct]?.bienLoiNhuan - 0)) /
        100) *
        (categories[bangTinh?.indexCategory]?.tyLeTrichChoKhachHang - 0)) /
      100 /
      12;
    const nChannel = channels[bangTinh?.indexChannel]?.tyLeDoiDiem;
    const nCategory = categories[bangTinh?.indexCategory]?.tyLeDoiDiem;
    const nProduct = listProduct[bangTinh?.indexProduct]?.tyLeDiem;
    const nSLProduct = 1;
    const nDoanhSoProduct = listProduct[bangTinh?.indexProduct]?.doanhSoSanPham
      ?.filter((tyle) => bangTinh?.soDu >= tyle.start)
      ?.pop()?.tyLeTheoDoanhSo;
    const nLaiSuatProduct =
      listProduct[bangTinh?.indexProduct]?.tyLeTheoLaiSuat;
    const res =
      (nLaiSuatProduct *
        nDoanhSoProduct *
        nSLProduct *
        nProduct *
        nCategory *
        nChannel *
        result) /
      bangTinh?.tyLeQuyDoiDiem;
    setPoint(res);
  };
  const handleUploadFile = (event) => {
    setFile({ fileSelected: event.target.files[0], loading: false });
  };
  const onSubmitFile = async () => {
    if (file.fileSelectd) {
      setFile({ ...file, loading: true });
      const data = new FormData();
      data.append("file", file.fileSelectd);
      const response = await uploadFileApi.Upload(data);
      setFile({ fileSelected: null, loading: false });
    }
  };
  return (
    <div className="container-fluid">
      {/* <!-- Content Row --> */}
      <div className="row">
        <div className="col-xl-12">
          <div className="card shadow">
            <div
              className="
            card-header
            py-3
            d-flex
            flex-row
            align-items-center
            justify-content-between
          "
            >
              <h6 className="m-0 font-weight-bold">Tính điểm cho khách hàng</h6>
            </div>
            <div className="card-body" style={{ background: "#fafafa" }}>
              <div
                className="col-md-8 align-self-center borderBox"
                style={{ marginBottom: "10px", textAlign: "right" }}
              >
                <input
                  type="file"
                  accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
                  onChange={handleUploadFile}
                />
                <button
                  type="button"
                  class="btn btn-success"
                  onClick={onSubmitFile}
                >
                  Upload
                </button>
              </div>
              <div
                className="col-md-8 align-self-center borderBox"
                style={{ marginBottom: "10px" }}
              >
                <div className="table-responsive">
                  <table
                    id="tb_chitiet"
                    className=" table table-striped table-bordered table-sm mb-0"
                    cellSpacing="0"
                  >
                    <thead>
                      <tr>
                        <th
                          className="th-sm text-center"
                          style={{ width: "100px" }}
                        >
                          STT
                        </th>
                        <th
                          className="th-sm text-center"
                          style={{ width: "200px" }}
                        >
                          Tên sản phẩm
                        </th>
                        <th
                          className="th-sm text-center"
                          style={{ width: "200px" }}
                        >
                          Số lượng SP sử dụng
                        </th>

                        <th
                          className="th-sm text-center"
                          style={{ width: "100px" }}
                        >
                          Tổng điểm
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      {summary.map((sum, index) => (
                        <tr>
                          <td className="text-center">{index + 1}</td>
                          <td>{sum?.product?.maSanPham}</td>
                          <td className="text-center">{sum.count}</td>
                          <td className="text-center">
                            {sum?.points?.reduce((cur, p) => {
                              return cur + p;
                            }, 0)}
                          </td>
                        </tr>
                      ))}
                      <tr>
                        <td className="text-center"></td>
                        <td></td>
                        <td className="text-center">Tổng</td>
                        <td className="text-center">
                          {users[bangTinh?.indexUser]?.totalPoints}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div
                className="col-md-8 align-self-center borderBox"
                style={{ margin: "0 auto" }}
              >
                {/* <!-- row --> */}
                <div className="row">
                  <div className="col-md-6">
                    <div className="item">
                      <label>CIF</label>
                      <div className="dropdown dropdown-border">
                        <select
                          className="selectpicker"
                          onChange={(e) => {
                            setBangTinh({
                              ...bangTinh,
                              indexUser: e.target.value,
                            });
                            (async () => {
                              const response = await historyApi.getAll({
                                user: users[e.target.value]?.id,
                              });
                              setSummary(response.data.data);
                            })();
                          }}
                          value={bangTinh?.indexUser}
                        >
                          {users.map((user, index) => (
                            <option
                              key={user.id}
                              data-tokens={user.name}
                              value={index}
                            >
                              {user.cif}
                            </option>
                          ))}
                        </select>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="item">
                      <label>Tên khách hàng</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Nhập vào thông tin"
                        readOnly
                        value={users[bangTinh?.indexUser]?.name}
                      />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="item">
                      <label>Kênh giao dịch</label>
                      <div className="dropdown dropdown-border">
                        <select
                          className="selectpicker"
                          onChange={(e) => {
                            setBangTinh({
                              ...bangTinh,
                              indexChannel: e.target.value,
                            });
                          }}
                          value={bangTinh?.indexChannel}
                        >
                          {channels.map((channel, index) => (
                            <option
                              key={channel.id}
                              data-tokens={channel.name}
                              value={index}
                            >
                              {channel.name}
                            </option>
                          ))}
                        </select>
                      </div>
                    </div>
                  </div>

                  <div className="col-md-6">
                    <div className="item">
                      <label>Loại sản phẩm</label>
                      <div className="dropdown dropdown-border">
                        <select
                          className="selectpicker"
                          onChange={(e) => {
                            const data = products.filter(
                              (product) =>
                                product.categoryId ===
                                categories[e.target.value]?.id
                            );
                            setBangTinh({
                              ...bangTinh,
                              indexCategory: e.target.value,
                              indexProduct: data.length > 0 ? 0 : -1,
                              soDu: bangTinh?.soDu || 0,
                            });
                            setListProduct(data);
                          }}
                          value={bangTinh?.indexCategory}
                        >
                          {categories.map((category, index) => (
                            <option value={index}>{category.name}</option>
                          ))}
                        </select>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="item">
                      <label>Tỷ lệ chi phí cho khách hàng (%)</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Nhập vào thông tin"
                        readOnly
                        value={
                          categories[bangTinh?.indexCategory]
                            ?.tyLeTrichChoKhachHang
                        }
                      />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="item">
                      <label>Tên sản phẩm</label>
                      <div className="dropdown dropdown-border">
                        <select
                          className="selectpicker"
                          data-live-search="true"
                          onChange={(e) => {
                            setBangTinh({
                              ...bangTinh,
                              indexProduct: e.target.value,
                            });
                          }}
                        >
                          {listProduct.map((product, index) => (
                            <option
                              data-tokens={product.maSanPham}
                              value={index}
                            >
                              {product.maSanPham}
                            </option>
                          ))}
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                {/* <!-- row --> */}
                <div className="row">
                  <div className="col-md-6">
                    <div className="item">
                      <label>Kỳ hạn / Thời hạn (tháng)</label>
                      <input
                        type="number"
                        className="form-control"
                        placeholder="Nhập vào thông tin"
                        readOnly
                        value={listProduct[bangTinh?.indexProduct]?.kyHan}
                      />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="item">
                      <label>Tỷ lệ theo số lượng</label>
                      <input
                        type="number"
                        className="form-control"
                        placeholder="Nhập vào thông tin"
                        readOnly
                        value={0}
                      />
                    </div>
                  </div>
                </div>
                {/* <!-- row --> */}
                <div className="row">
                  <div className="col-md-6">
                    <div className="item">
                      <label>Số dư gửi/ Dư nợ</label>
                      <input
                        type="number"
                        className="form-control"
                        placeholder="Nhập vào thông tin"
                        value={bangTinh?.soDu}
                        defaultValue={0}
                        onChange={(e) => {
                          setBangTinh({ ...bangTinh, soDu: e.target.value });
                        }}
                      />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="item">
                      <label>Tỷ lệ theo số dư gửi/ Dư nợ</label>
                      <input
                        type="number"
                        readOnly
                        className="form-control"
                        placeholder="Nhập vào thông tin"
                        value={
                          listProduct[bangTinh?.indexProduct]?.doanhSoSanPham
                            ?.filter((tyle) => bangTinh?.soDu >= tyle.start)
                            ?.pop()?.tyLeTheoDoanhSo
                        }
                      />
                    </div>
                  </div>
                </div>

                {/* <!-- row --> */}
                <div className="row">
                  <div className="col-md-6">
                    <div className="item">
                      <label>Lãi suất (%)</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Nhập vào thông tin"
                        value={listProduct[bangTinh?.indexProduct]?.laiSuat}
                        readOnly
                      />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="item">
                      <label style={{ display: "block" }}>
                        Tỷ lệ lãi suất (%)
                      </label>
                      <span style={{ marginLeft: 0 }}>
                        <input
                          style={{ marginRight: 5 }}
                          onChange={(e) => {
                            setSelectLaiSuat(e.target.value);
                          }}
                          type="radio"
                          name="radio"
                          value="1"
                          defaultChecked
                          id="check1"
                        />
                        <label class="form-check-label" for="check1">
                          Cộng lãi xuất
                        </label>
                      </span>
                      <span style={{ marginLeft: 20 }}>
                        <input
                          style={{ marginRight: 5 }}
                          onChange={(e) => {
                            setSelectLaiSuat(e.target.value);
                          }}
                          type="radio"
                          name="radio"
                          value="2"
                          id="check1"
                        />
                        <label class="form-check-label" for="check1">
                          Không cộng lãi xuất
                        </label>
                      </span>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Nhập vào thông tin"
                        value={
                          selectLaiSuat === "1"
                            ? listProduct[bangTinh?.indexProduct]
                                ?.tyLeTheoLaiSuat?.congLaiSuat
                            : listProduct[bangTinh?.indexProduct]
                                ?.tyLeTheoLaiSuat?.khongCongLaiSuat
                        }
                        readOnly
                      />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="item">
                      <label>Biên lợi nhuận (%)</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Nhập vào thông tin"
                        value={
                          listProduct[bangTinh?.indexProduct]?.bienLoiNhuan
                        }
                        readOnly
                      />
                    </div>
                  </div>
                </div>
                {/* <!-- row --> */}
                <div className="row">
                  <div className="col-md-6">
                    <div className="item">
                      <label>Tỷ lệ quy đổi điểm</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Nhập vào thông tin"
                        value={bangTinh?.tyLeQuyDoiDiem || 0}
                        onChange={(e) => {
                          setBangTinh({
                            ...bangTinh,
                            tyLeQuyDoiDiem: e.target.value,
                          });
                        }}
                      />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="item">
                      <label>Chi phí trích cho khách hàng</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Nhập vào thông tin"
                        readOnly
                        value={
                          ((((bangTinh?.soDu - 0) *
                            (listProduct[bangTinh?.indexProduct]?.kyHan - 0) *
                            (listProduct[bangTinh?.indexProduct]?.bienLoiNhuan -
                              0)) /
                            100) *
                            (categories[bangTinh?.indexCategory]
                              ?.tyLeTrichChoKhachHang -
                              0)) /
                          100 /
                          12
                        }
                      />
                    </div>
                  </div>
                </div>
                {/* <!-- Divider --> */}
                <hr className="sidebar-divider" />
                {/* <!-- row --> */}
                <div className="row">
                  <div className="col-md-6">
                    <div>
                      <label>Số điểm của khách hàng</label>
                      <input
                        type="text"
                        className="
                      form-control
                      text-success
                      font-weight-bold
                      text-right
                    "
                        placeholder="Điểm sau khi tính"
                        value={point}
                        readOnly
                      />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="d-flex align-items-end h-100">
                      <button
                        onClick={async () => {
                          const data = await historyApi.Create({
                            user: users[bangTinh?.indexUser].id,
                            product: products[bangTinh?.indexProduct].id,
                            channelId: channels[bangTinh?.indexChannel].id,
                            soDus: bangTinh?.soDu,
                            selectLaiSuat,
                          });
                          (async () => {
                            const response = await historyApi.getAll({
                              user: users[bangTinh?.indexUser]?.id,
                            });
                            setSummary(response.data.data);
                            console.log(response.data.data);
                          })();
                          setPoint(data?.data?.data);
                        }}
                        type="button"
                        className="btn btn-primary btn-sm minW100"
                        style={{ minHeight: 38 }}
                      >
                        <a>Tính điểm</a>
                      </button>
                    </div>
                  </div>
                </div>
                {/* <!-- Divider --> */}
                <hr className="sidebar-divider" />
                {/* <!-- row --> */}
                <div className="row"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

TinhDiem.propTypes = {};

export default TinhDiem;
