export class productModel {
  constructor({
    name,
    maSanPham,
    soLuongSuDung,
    doanhSoSanPham,
    laiSuat,
    tyLeTheoLaiSuat,
    categoryId,
    kyHan,
    bienLoiNhuan,
  }) {
    this.name = name || "";
    this.maSanPham = maSanPham;
    this.soLuongSuDung = soLuongSuDung;
    this.doanhSoSanPham = doanhSoSanPham;
    this.laiSuat = laiSuat;
    this.tyLeTheoLaiSuat = tyLeTheoLaiSuat;
    this.categoryId = categoryId;
    this.kyHan = kyHan;
    this.bienLoiNhuan = bienLoiNhuan;
  }
  name = "";
  maSanPham = "";
  tyLeDiem = "";
  soLuongSuDung = {};
  doanhSoSanPham = [];
  laiSuat = "";
  tyLeTheoLaiSuat = "";
  categoryId = "";
  kyHan = "";
  bienLoiNhuan = "";
}
