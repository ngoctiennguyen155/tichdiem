import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import productApi from "../../client/api/product";
import channelApi from "../../client/api/channel";
import categoryApi from "../../client/api/catalogy";
const Tables = (props) => {
  const [tab, setTab] = useState(1);
  const [dataSet, setDateSet] = useState([]);
  const [showModal, setShowModal] = useState({
    addChannel: false,
    addProduct: false,
    addCategory: false,
  });
  const [channelInfo, setChannelInfo] = useState({
    name: "",
    maChannel: "",
    tyLeDoiDiem: "",
  });
  const [categoryInfo, setCategoryInfo] = useState({
    name: "",
    tyLeDoiDiem: "",
    tyLeTrichChoKhachHang: "",
  });
  const [productInfo, setProductInfo] = useState({
    name: "",
    maSanPham: "",
    tyLeDiem: "",
    soLuongSuDung: { selectionType: 1, soLuong: [], tyLeTheoSoLuong: "" },
    doanhSoSanPham: [],
    laiSuat: "",
    tyLeTheoLaiSuat: { congLaiSuat: "", khongCongLaiSuat: "" },
    categoryId: "",
    kyHan: "",
    bienLoiNhuan: "",
  });
  const [channels, setChannels] = useState([]);
  const [categories, setCategories] = useState([]);

  const [update, setUpdate] = useState({
    channel: false,
    category: false,
    product: false,
  });
  const [option, setOption] = useState({});
  useEffect(() => {
    // fetch data tables
    (async () => {
      try {
        const fetProducts = await productApi.getAll();
        const formatData = fetProducts.data.data.map((e) => {
          let cloneSP = { ...e };
          cloneSP = Object.assign(e, { id: e._id });
          cloneSP.doanhSoSanPham = e.doanhSoSanPham.map((ds) => {
            return Object.assign({}, { id: ds._id }, ds);
          });
          cloneSP.soLuongSuDung.soLuong = [
            ...e.soLuongSuDung.soLuong.map((sl) => {
              return Object.assign({}, { id: sl._id }, sl);
            }),
          ];
          return cloneSP;
        });
        setDateSet(formatData);
      } catch (error) {}
    })();
    // fetch channels
    (async () => {
      try {
        const response = await channelApi.getAll();
        const formatData = response.data.data.map((e) => {
          return Object.assign({}, { id: e._id }, e);
        });
        setChannels(formatData);
      } catch (error) {}
    })();
    // fetch categories
    (async () => {
      try {
        const response = await categoryApi.getAll();
        console.log({ response });
        const formatData = response.data.data.map((e) => {
          return Object.assign({}, { id: e._id }, e);
        });
        setCategories(formatData);
        if (formatData.length > 0) {
          setTab(formatData[0]._id);
        }
      } catch (error) {}
    })();
  }, []);

  return (
    <div>
      <div className="container-fluid">
        {/* <!-- Content Row --> */}
        {/* <!-- row --> */}
        <div className="row">
          <div className="col-md-12">
            <div className="d-flex align-items-end justify-content-end h-100">
              {/* button */}
              <div className="item">
                <button
                  type="button"
                  className="btn btn-primary btn-sm minW100 mr-2"
                  onClick={() => {
                    setShowModal({ ...showModal, addChannel: true });
                  }}
                >
                  <a
                    href="#"
                    data-toggle="modal"
                    data-target="#themKenhGiaoDich"
                  >
                    <i className="fab fa-buffer fa-sm fa-fw mr-1 text-gray-400"></i>
                    Thêm kênh giao dịch
                  </a>
                </button>
                <button
                  type="button"
                  className="btn btn-primary btn-sm minW100 mr-2"
                  onClick={() => {
                    setShowModal({ ...showModal, addCategory: true });
                  }}
                >
                  <a
                    href="#"
                    data-toggle="modal"
                    data-target="#themKenhGiaoDich"
                  >
                    <i className="fab fa-buffer fa-sm fa-fw mr-1 text-gray-400"></i>
                    Thêm loại sản phẩm
                  </a>
                </button>
                <button
                  onClick={() => {
                    setShowModal({ ...showModal, addProduct: true });
                  }}
                  type="button"
                  className="btn btn-primary btn-sm minW100"
                >
                  <a href="#" data-toggle="modal" data-target="#themSanPham">
                    <i className="fas fa-box fa-sm fa-fw mr-1 text-gray-400"></i>
                    Thêm sản phẩm
                  </a>
                </button>
              </div>
            </div>
          </div>
        </div>
        {/* <!-- row --> */}
        <div className="row">
          <div className="col-xl-12">
            <div className="card shadow">
              <div
                className="
                      card-header
                      d-flex
                      flex-row
                      align-items-center
                      justify-content-between
                      py-3
                    "
              >
                <h6 className="m-0 font-weight-bold ">Danh sách sản phẩm</h6>
              </div>
              <div className="card-body">
                <ul className="nav nav-tabs" id="myTab" role="tablist">
                  {categories.map((category) => (
                    <li
                      className="nav-item"
                      onClick={() => {
                        setTab(category._id);
                      }}
                    >
                      <a
                        className={
                          tab === category.id ? "nav-link active" : "nav-link"
                        }
                        id="tiengui-tab"
                        data-toggle="tab"
                        href="#tiengui"
                        role="tab"
                        aria-controls="tiengui"
                        aria-selected="true"
                      >
                        {category.name}
                      </a>
                    </li>
                  ))}
                </ul>
                <div className="tab-content" id="mainTabContent">
                  <div
                    className="tab-pane fade show active"
                    id="tiengui"
                    role="tabpanel"
                    aria-labelledby="tiengui-tab"
                  >
                    {/* <!-- row --> */}
                    <div className="row">
                      <div className="col-xl-12 col-md-12">
                        <div className="table-responsive">
                          <table
                            id="tb_tiengui"
                            className="table table-striped table-bordered table-sm"
                            cellSpacing="0"
                          >
                            <thead>
                              <tr>
                                <th
                                  className="th-sm text-center"
                                  style={{ width: "100px" }}
                                >
                                  Mã sản phẩm
                                </th>
                                <th
                                  className="th-sm text-center"
                                  style={{ width: "200px" }}
                                >
                                  Tên sản phẩm
                                </th>
                                <th
                                  className="th-sm text-center"
                                  style={{ width: "200px" }}
                                >
                                  Tỷ lệ điểm theo MSP
                                </th>
                                <th
                                  className="th-sm text-center"
                                  style={{ width: "300px" }}
                                >
                                  Số lượng SP sử dụng
                                </th>
                                <th
                                  className="th-sm text-center"
                                  style={{ width: "400px" }}
                                >
                                  Doanh số SP
                                </th>
                                <th
                                  className="th-sm text-center"
                                  style={{ width: "150px" }}
                                >
                                  Lãi suất
                                </th>
                                <th
                                  className="th-sm text-center"
                                  style={{ width: "150px" }}
                                >
                                  Tỷ lệ theo lãi suất
                                </th>
                                <th
                                  className="th-sm text-center"
                                  style={{ width: "100px" }}
                                >
                                  Kỳ hạn
                                </th>
                                <th
                                  className="th-sm text-center"
                                  style={{ width: "150px" }}
                                >
                                  Biên lợi nhuận
                                </th>
                                <th
                                  className="th-sm text-center"
                                  style={{ width: "200px" }}
                                >
                                  Thao tác
                                </th>
                              </tr>
                            </thead>
                            <tbody>
                              {dataSet
                                .filter((e) => e.categoryId === tab)
                                .map((e, i) => (
                                  <tr key={i}>
                                    <td className="text-center">
                                      {e.maSanPham}
                                    </td>
                                    <td className="text-center"> {e.name}</td>
                                    <td className="text-center">
                                      {e.tyLeDiem}
                                    </td>
                                    <td className="text-center">
                                      {e.soLuongSuDung.selectionType === 1 ? (
                                        e.soLuongSuDung.soLuong.map((sl) => (
                                          <p>
                                            Từ {sl.start} đến{" "}
                                            {sl.end ? sl.end : `>= ${sl.start}`}
                                            : {sl.tyLeTheoSoLuong}
                                          </p>
                                        ))
                                      ) : (
                                        <p>
                                          Tỷ lệ bậc thang:{" "}
                                          {e.soLuongSuDung.tyLeTheoSoLuong}
                                        </p>
                                      )}
                                    </td>
                                    <td className="text-center">
                                      {e.doanhSoSanPham.map((ds) => (
                                        <p>
                                          Từ {ds.start} đến{" "}
                                          {ds.end ? ds.end : ` >${ds.start}`}:{" "}
                                          {ds.tyLeTheoDoanhSo}
                                        </p>
                                      ))}
                                    </td>
                                    <td className="text-center">
                                      <p>{e.laiSuat}</p>
                                    </td>
                                    <td className="text-center">
                                      <p>
                                        Cộng lãi suất:{" "}
                                        {e.tyLeTheoLaiSuat.congLaiSuat}
                                      </p>
                                      <p>
                                        Không lãi suất:{" "}
                                        {e.tyLeTheoLaiSuat.khongCongLaiSuat}
                                      </p>
                                    </td>
                                    <td className="text-center">{e.kyHan}</td>
                                    <td className="text-center">
                                      {e.bienLoiNhuan}
                                    </td>
                                    <td className="text-center">
                                      <a
                                        style={{ cursor: "pointer" }}
                                        onClick={async () => {
                                          try {
                                            const deleteProduc =
                                              await productApi.delete(e.id);
                                            setDateSet(
                                              dataSet.filter(
                                                (ee) => ee.id !== e.id
                                              )
                                            );
                                          } catch (error) {}
                                        }}
                                      >
                                        Xóa
                                      </a>{" "}
                                      |{" "}
                                      <a
                                        style={{ cursor: "pointer" }}
                                        onClick={() => {
                                          setUpdate({
                                            ...update,
                                            product: true,
                                          });
                                          setProductInfo(e);
                                          setShowModal({
                                            ...showModal,
                                            addProduct: true,
                                          });
                                        }}
                                      >
                                        Sửa
                                      </a>
                                    </td>
                                  </tr>
                                ))}
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* <!-- Thêm kênh giao dịch Modal--> */}
      <div
        className={
          showModal.addChannel ? `modal modal-add fade show` : `modal fade`
        }
        id="themKenhGiaoDich"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                {update.channel
                  ? "Cập nhật kênh giao dịch"
                  : "Thêm kênh giao dịch"}
              </h5>
              <button
                className="close"
                type="button"
                data-dismiss="modal"
                aria-label="Close"
                onClick={() => {
                  setShowModal({ ...showModal, addChannel: false });
                  setUpdate({ ...update, channel: false });
                  setChannelInfo({ name: "", tyLeDoiDiem: "", maChannel: "" });
                }}
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">
              {/* <!-- row --> */}
              <div className="row">
                <div className="col-md-6">
                  <div className="item">
                    <label>Kênh giao dịch</label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Nhập vào thông tin"
                      value={channelInfo.name}
                      onChange={(e) => {
                        setChannelInfo({
                          ...channelInfo,
                          name: e.target.value,
                        });
                      }}
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="item">
                    <label>Mã kênh giao dịch</label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Nhập vào thông tin"
                      value={channelInfo.maChannel}
                      onChange={(e) => {
                        setChannelInfo({
                          ...channelInfo,
                          maChannel: e.target.value,
                        });
                      }}
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="item">
                    <label>Tỷ lệ điểm</label>
                    <input
                      type="number"
                      className="form-control"
                      placeholder="Nhập vào thông tin"
                      value={channelInfo.tyLeDoiDiem}
                      onChange={(e) => {
                        setChannelInfo({
                          ...channelInfo,
                          tyLeDoiDiem: e.target.value,
                        });
                      }}
                    />
                  </div>
                </div>
                <div className="col-xl-12 col-md-12">
                  <div className="d-flex align-items-end justify-content-end h-100">
                    <div className="item">
                      <button
                        type="button"
                        className="btn btn-success btn-sm minW100"
                        onClick={async () => {
                          try {
                            if (update.channel) {
                              const updateChannel = await channelApi.Update(
                                channelInfo.id,
                                channelInfo
                              );
                              if (updateChannel) {
                                let cloneChannels = [...channels];
                                const index = channels
                                  .map((e) => e.id)
                                  .indexOf(channelInfo.id);
                                if (index !== -1) {
                                  cloneChannels[index].name = channelInfo.name;
                                  cloneChannels[index].maChannel =
                                    channelInfo.maChannel;
                                  cloneChannels[index].tyLeDoiDiem =
                                    channelInfo.tyLeDoiDiem;
                                  setChannels([...cloneChannels]);
                                  setUpdate({ ...update, channel: false });
                                }
                              }
                            } else {
                              const addChannel = await channelApi.Create(
                                channelInfo
                              );
                              if (addChannel) {
                                setChannels([
                                  ...channels,
                                  Object.assign(
                                    {},
                                    { id: addChannel._id },
                                    channelInfo
                                  ),
                                ]);
                              }
                            }
                          } catch (error) {
                          } finally {
                            setChannelInfo({
                              name: "",
                              tyLeDoiDiem: "",
                              maChannel: "",
                            });
                          }
                        }}
                      >
                        <a href="#">{update.channel ? "Cập nhật" : "Thêm"} </a>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-xl-12 col-md-12">
                  <div className="table-responsive">
                    <table
                      id="dtBasicExample"
                      className="table table-striped table-bordered table-sm"
                      cellSpacing="0"
                    >
                      <thead>
                        <tr>
                          <th className="th-sm text-center">Kênh giao dịch</th>
                          <th className="th-sm text-center">
                            Mã kênh giao dịch
                          </th>
                          <th className="th-sm text-center">Tỉ lệ điểm</th>
                          <th className="th-sm text-center">Thao tác</th>
                        </tr>
                      </thead>
                      <tbody>
                        {channels.map((channel) => (
                          <tr>
                            <td>{channel.name}</td>
                            <td>{channel?.maChannel}</td>
                            <td className="text-center">
                              {channel.tyLeDoiDiem}
                            </td>
                            <td className="text-center">
                              <a
                                style={{ cursor: "pointer" }}
                                onClick={async () => {
                                  try {
                                    const deleleChanel =
                                      await channelApi.delete(channel.id);
                                    setChannels(
                                      channels.filter(
                                        (e) => e.id !== channel.id
                                      )
                                    );
                                  } catch (error) {}
                                }}
                              >
                                Xóa
                              </a>
                              |{" "}
                              <a
                                style={{ cursor: "pointer" }}
                                onClick={() => {
                                  setUpdate({ ...update, channel: true });
                                  setShowModal({
                                    ...showModal,
                                    addChannel: true,
                                  });
                                  setChannelInfo(channel);
                                }}
                              >
                                {" "}
                                Sửa
                              </a>
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div className="modal-footer">
              <button
                className="btn btn-secondary"
                type="button"
                data-dismiss="modal"
                aria-label="Close"
                onClick={() => {
                  setShowModal({ ...showModal, addChannel: false });
                  setUpdate({ ...update, channel: false });
                  setChannelInfo({ name: "", tyLeDoiDiem: "", maChannel: "" });
                }}
              >
                Thoát
              </button>
            </div>
          </div>
        </div>
      </div>
      {/* // them loai san pham */}
      <div
        className={
          showModal.addCategory ? `modal  modal-add fade show` : `modal fade`
        }
        id="themKenhGiaoDich"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                {update.category
                  ? "Cập nhật loại sản phẩm"
                  : "Thêm loại sản phẩm"}
              </h5>
              <button
                className="close"
                type="button"
                data-dismiss="modal"
                aria-label="Close"
                onClick={() => {
                  setShowModal({ ...showModal, addCategory: false });
                  setUpdate({ ...update, category: false });
                  setCategoryInfo({
                    name: "",
                    tyLeDoiDiem: "",
                    tyLeTrichChoKhachHang: "",
                  });
                }}
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">
              {/* <!-- row --> */}
              <div className="row">
                <div className="col-md-6">
                  <div className="item">
                    <label>Loại sản phẩm</label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Nhập vào thông tin"
                      value={categoryInfo.name}
                      onChange={(e) => {
                        setCategoryInfo({
                          ...categoryInfo,
                          name: e.target.value,
                        });
                      }}
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="item">
                    <label>Tỷ lệ điểm</label>
                    <input
                      type="number"
                      className="form-control"
                      placeholder="Nhập vào thông tin"
                      value={categoryInfo.tyLeDoiDiem}
                      onChange={(e) => {
                        setCategoryInfo({
                          ...categoryInfo,
                          tyLeDoiDiem: e.target.value,
                        });
                      }}
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="item">
                    <label>Tỷ lệ trích cho khách hàng </label>
                    <input
                      type="number"
                      className="form-control"
                      placeholder="Nhập vào thông tin"
                      value={categoryInfo.tyLeTrichChoKhachHang}
                      onChange={(e) => {
                        setCategoryInfo({
                          ...categoryInfo,
                          tyLeTrichChoKhachHang: e.target.value,
                        });
                      }}
                    />
                  </div>
                </div>
                <div className="col-xl-12 col-md-12">
                  <div className="d-flex align-items-end justify-content-end h-100">
                    <div className="item">
                      <button
                        type="button"
                        className="btn btn-success btn-sm minW100"
                        onClick={async () => {
                          try {
                            if (update.category) {
                              const updateCategory = await categoryApi.Update(
                                categoryInfo.id,
                                {
                                  name: categoryInfo.name,
                                  tyLeDoiDiem: categoryInfo.tyLeDoiDiem,
                                  tyLeTrichChoKhachHang:
                                    categoryInfo.tyLeTrichChoKhachHang,
                                }
                              );
                              if (updateCategory) {
                                setUpdate({ ...update, category: false });
                                let cloneCategories = [...categories];
                                const index = categories
                                  .map((e) => e.id)
                                  .indexOf(categoryInfo.id);
                                if (index !== -1) {
                                  cloneCategories[index].name =
                                    categoryInfo.name;
                                  cloneCategories[index].tyLeDoiDiem =
                                    categoryInfo.tyLeDoiDiem;
                                  cloneCategories[index].tyLeTrichChoKhachHang =
                                    categoryInfo.tyLeTrichChoKhachHang;
                                  setCategories(cloneCategories);
                                }
                              }
                            } else {
                              const addChannel = await categoryApi.Create(
                                categoryInfo
                              );
                              if (addChannel) {
                                setCategories([
                                  ...categories,
                                  Object.assign(
                                    {},
                                    { id: addChannel.data.data._id },
                                    categoryInfo
                                  ),
                                ]);
                              }
                            }
                          } catch (error) {
                          } finally {
                            setCategoryInfo({
                              name: "",
                              tyLeDoiDiem: "",
                              tyLeTrichChoKhachHang: "",
                            });
                          }
                        }}
                      >
                        <a href="#">{update.category ? "Cập nhật" : "Thêm"} </a>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-xl-12 col-md-12">
                  <div className="table-responsive">
                    <table
                      id="dtBasicExample"
                      className="table table-striped table-bordered table-sm"
                      cellSpacing="0"
                    >
                      <thead>
                        <tr>
                          <th className="th-sm text-center">Kênh giao dịch</th>
                          <th className="th-sm text-center">Tỉ lệ điểm</th>
                          <th className="th-sm text-center">
                            Tỷ lệ trích cho KH
                          </th>
                          <th className="th-sm text-center">Thao tác</th>
                        </tr>
                      </thead>
                      <tbody>
                        {categories.map((channel) => (
                          <tr>
                            <td>{channel.name}</td>
                            <td className="text-center">
                              {channel.tyLeDoiDiem}
                            </td>
                            <td className="text-center">
                              {channel.tyLeTrichChoKhachHang}
                            </td>
                            <td className="text-center">
                              <a
                                style={{ cursor: "pointer" }}
                                onClick={async () => {
                                  try {
                                    const deleleChanel =
                                      await categoryApi.delete(channel.id);
                                    setCategories(
                                      categories.filter(
                                        (e) => e.id !== channel.id
                                      )
                                    );
                                  } catch (error) {}
                                }}
                              >
                                Xóa
                              </a>
                              |{" "}
                              <a
                                style={{ cursor: "pointer" }}
                                onClick={() => {
                                  setUpdate({ ...update, category: true });
                                  setShowModal({
                                    ...showModal,
                                    addCategory: true,
                                  });
                                  setCategoryInfo(channel);
                                }}
                              >
                                Sửa
                              </a>
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div className="modal-footer">
              <button
                className="btn btn-secondary"
                type="button"
                data-dismiss="modal"
                aria-label="Close"
                onClick={() => {
                  setShowModal({ ...showModal, addCategory: false });
                  setUpdate({ ...update, category: false });
                  setCategoryInfo({
                    name: "",
                    tyLeDoiDiem: "",
                    tyLeTrichChoKhachHang: "",
                  });
                }}
              >
                Thoát
              </button>
            </div>
          </div>
        </div>
      </div>
      {/* <!-- Thêm sản phẩm Modal--> */}
      <div
        className={
          showModal.addProduct ? `modal modal-add fade show` : `modal fade`
        }
        id="themSanPham"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                {update.product ? "Cập nhật sản phẩm" : "Thêm sản phẩm"}
              </h5>
              <button
                className="close"
                type="button"
                data-dismiss="modal"
                aria-label="Close"
                onClick={() => {
                  setShowModal({ ...showModal, addProduct: false });
                  setUpdate({ ...update, product: false });
                  setProductInfo({
                    name: "",
                    maSanPham: "",
                    tyLeDiem: "",
                    soLuongSuDung: {
                      selectionType: 1,
                      soLuong: [],
                      tyLeTheoSoLuong: "",
                    },
                    doanhSoSanPham: [],
                    laiSuat: "",
                    tyLeTheoLaiSuat: {
                      congLaiSuat: "",
                      khongCongLaiSuat: "",
                    },
                    categoryId: "",
                    kyHan: "",
                    bienLoiNhuan: "",
                  });
                }}
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body modal-body-scroll">
              {/* <!-- row --> */}
              <div className="row">
                <div className="col-md-6">
                  <div className="item">
                    <label>Mã sản phẩm</label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Nhập vào thông tin"
                      value={productInfo.maSanPham}
                      onChange={(e) => {
                        setProductInfo({
                          ...productInfo,
                          maSanPham: e.target.value,
                        });
                      }}
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="item">
                    <label>Tên sản phẩm</label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Nhập vào thông tin"
                      value={productInfo.name}
                      onChange={(e) => {
                        setProductInfo({
                          ...productInfo,
                          name: e.target.value,
                        });
                      }}
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="item">
                    <label>Loại sản phẩm</label>
                    <div className="dropdown dropdown-border">
                      <select
                        className="selectpicker"
                        data-live-search="true"
                        value={productInfo?.categoryId}
                        onChange={(e) => {
                          setProductInfo({
                            ...productInfo,
                            categoryId: e.target.value,
                          });
                        }}
                      >
                        {categories.map((category) => (
                          <option data-tokens="Tiền gửi" value={category.id}>
                            {category.name}
                          </option>
                        ))}
                      </select>
                    </div>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="item">
                    <label>Tỷ lệ điểm theo mã sản phẩm</label>
                    <input
                      type="number"
                      className="form-control"
                      placeholder="Nhập vào thông tin"
                      value={productInfo.tyLeDiem}
                      onChange={(e) => {
                        setProductInfo({
                          ...productInfo,
                          tyLeDiem: e.target.value,
                        });
                      }}
                    />
                  </div>
                </div>
              </div>
              <div className="box-in-modal">
                <p className="title">Bảng tỷ lệ SL sản phẩm sử dụng</p>
                <div className="col-md-12">
                  {/* <!-- Thêm Bảng số lượng sản phẩm sử dụng--> */}
                  <div style={{ marginBottom: 10 }}>
                    <span style={{ marginRight: 20 }}>
                      <input
                        style={{ marginRight: 5 }}
                        onChange={(e) => {
                          setProductInfo({
                            ...productInfo,
                            soLuongSuDung: {
                              ...productInfo.soLuongSuDung,
                              selectionType: e.target.value - 0,
                            },
                          });
                        }}
                        type="radio"
                        name="radio"
                        defaultChecked
                        value="1"
                        id="check1"
                      />
                      <label class="form-check-label" for="check1">
                        Tỷ lệ theo số
                      </label>
                    </span>

                    <span>
                      <input
                        style={{ marginRight: 5 }}
                        onChange={(e) => {
                          setProductInfo({
                            ...productInfo,
                            soLuongSuDung: {
                              ...productInfo.soLuongSuDung,
                              selectionType: e.target.value - 0,
                            },
                          });
                        }}
                        type="radio"
                        name="radio"
                        value="2"
                        id="check2"
                      />
                      <label class="form-check-label" for="check2">
                        Tỷ lệ theo bậc thang
                      </label>
                    </span>
                  </div>

                  {/* <!-- row --> */}
                  <div className="row">
                    {productInfo.soLuongSuDung.selectionType === 2 ? (
                      <div className="col-md-12">
                        <div className="item">
                          <label>Tỷ lệ số lượng theo bậc thang</label>
                          <input
                            type="number"
                            className="form-control"
                            placeholder="Nhập vào thông tin"
                            value={productInfo?.soLuongSuDung?.tyLeTheoSoLuong}
                            onChange={(e) => {
                              setProductInfo({
                                ...productInfo,
                                soLuongSuDung: {
                                  ...productInfo.soLuongSuDung,
                                  tyLeTheoSoLuong: e.target.value,
                                },
                              });
                            }}
                          />
                        </div>
                      </div>
                    ) : (
                      <>
                        <div className="col-md-4">
                          <div className="item">
                            <label>Số lượng sản phẩm min</label>
                            <input
                              type="number"
                              className="form-control"
                              placeholder="Nhập vào thông tin"
                              value={option?.soLuongSanPhamMin}
                              onChange={(e) => {
                                setOption({
                                  ...option,
                                  soLuongSanPhamMin: e.target.value,
                                });
                              }}
                            />
                          </div>
                        </div>
                        <div className="col-md-4">
                          <div className="item">
                            <label>Số lượng sản phẩm max</label>
                            <input
                              type="number"
                              className="form-control"
                              placeholder="Nhập vào thông tin"
                              value={option?.soLuongSanPhamMax}
                              onChange={(e) => {
                                setOption({
                                  ...option,
                                  soLuongSanPhamMax: e.target.value,
                                });
                              }}
                            />
                          </div>
                        </div>
                        <div className="col-md-4">
                          <div className="item">
                            <label>Tỷ lệ theo số lượng sử dụng</label>
                            <input
                              type="number"
                              className="form-control"
                              placeholder="Nhập vào thông tin"
                              value={option?.tyLeTheoSoLuong}
                              onChange={(e) => {
                                setOption({
                                  ...option,
                                  tyLeTheoSoLuong: e.target.value,
                                });
                              }}
                            />
                          </div>
                        </div>

                        <div className="col-xl-12 col-md-12">
                          <div className="d-flex align-items-end justify-content-end h-100">
                            <div className="item">
                              <button
                                className="btn btn-success"
                                type="button"
                                data-dismiss="modal"
                                onClick={() => {
                                  if (
                                    option?.soLuongSanPhamMin &&
                                    option?.tyLeTheoSoLuong
                                  ) {
                                    if (update?.soLuong) {
                                      let arrSoLuong = [
                                        ...productInfo.soLuongSuDung.soLuong,
                                      ];
                                      const indexUpdate = arrSoLuong
                                        .map((e) => e.id)
                                        .indexOf(update.idSoLuong);
                                      if (indexUpdate !== -1) {
                                        arrSoLuong[indexUpdate] = {
                                          ...arrSoLuong[indexUpdate],
                                          start: option.soLuongSanPhamMin,
                                          end: option.soLuongSanPhamMax,
                                          tyLeTheoSoLuong:
                                            option.tyLeTheoSoLuong,
                                        };
                                        setProductInfo({
                                          ...productInfo,
                                          soLuongSuDung: {
                                            ...productInfo.soLuongSuDung,
                                            soLuong: arrSoLuong,
                                          },
                                        });
                                        setUpdate({
                                          ...update,
                                          soLuong: false,
                                          idSoLuong: "",
                                        });
                                      }
                                    } else {
                                      setProductInfo({
                                        ...productInfo,
                                        soLuongSuDung: {
                                          ...productInfo.soLuongSuDung,
                                          soLuong: [
                                            ...productInfo.soLuongSuDung.soLuong.concat(
                                              [
                                                {
                                                  id:
                                                    productInfo.soLuongSuDung
                                                      .soLuong.length + 1,
                                                  start:
                                                    option.soLuongSanPhamMin,
                                                  end: option.soLuongSanPhamMax,
                                                  tyLeTheoSoLuong:
                                                    option.tyLeTheoSoLuong,
                                                },
                                              ]
                                            ),
                                          ],
                                        },
                                      });
                                    }
                                    setOption({
                                      ...option,
                                      soLuongSanPhamMax: "",
                                      soLuongSanPhamMin: "",
                                      tyLeTheoSoLuong: "",
                                    });
                                  }
                                }}
                              >
                                {update?.soLuong ? "Cập nhật" : "Thêm"}
                              </button>
                            </div>
                          </div>
                        </div>

                        <div className="col-xl-12 col-md-12">
                          <div className="table-responsive">
                            <table
                              id="dtBasicExample"
                              className="table table-striped table-bordered table-sm"
                              cellSpacing="0"
                            >
                              <thead>
                                <tr>
                                  <th className="th-sm text-center">
                                    Số lượng SP min
                                  </th>
                                  <th className="th-sm text-center">
                                    Số lượng SP max
                                  </th>
                                  <th className="th-sm text-center">
                                    Tỷ lệ theo SL sử dụng
                                  </th>
                                  <th className="th-sm text-center">
                                    Thao tác
                                  </th>
                                </tr>
                              </thead>
                              <tbody>
                                {productInfo.soLuongSuDung?.soLuong?.map(
                                  (sl) => (
                                    <tr>
                                      <td>{sl.start}</td>
                                      <td>{sl.end}</td>
                                      <td>{sl.tyLeTheoSoLuong}</td>
                                      <td className="text-center">
                                        <a
                                          style={{ cursor: "pointer" }}
                                          onClick={() => {
                                            setProductInfo({
                                              ...productInfo,
                                              soLuongSuDung: {
                                                ...productInfo.soLuongSuDung,
                                                soLuong: [
                                                  ...productInfo.soLuongSuDung.soLuong.filter(
                                                    (e) => e.id !== sl.id
                                                  ),
                                                ],
                                              },
                                            });
                                          }}
                                        >
                                          Xóa
                                        </a>{" "}
                                        |{" "}
                                        <a
                                          style={{ cursor: "pointer" }}
                                          onClick={() => {
                                            setOption({
                                              ...option,
                                              soLuongSanPhamMin: sl.start,
                                              soLuongSanPhamMax: sl.end,
                                              tyLeTheoSoLuong:
                                                sl.tyLeTheoSoLuong,
                                            });
                                            setUpdate({
                                              ...update,
                                              soLuong: true,
                                              idSoLuong: sl.id,
                                            });
                                          }}
                                        >
                                          Sửa
                                        </a>
                                      </td>
                                    </tr>
                                  )
                                )}
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </>
                    )}
                  </div>
                </div>
              </div>
              <div className="box-in-modal">
                <p className="title">Bảng doanh số sản phẩm</p>
                <div className="col-md-12">
                  {/* <!-- Thêm range doanh số sản phẩm--> */}

                  {/* <!-- row --> */}
                  <div className="row">
                    <div className="col-md-4">
                      <div className="item">
                        <label>Doanh số min</label>
                        <input
                          type="number"
                          className="form-control"
                          placeholder="Nhập vào thông tin"
                          value={option?.doanhSoSanPhamMin}
                          onChange={(e) => {
                            setOption({
                              ...option,
                              doanhSoSanPhamMin: e.target.value,
                            });
                          }}
                        />
                      </div>
                    </div>
                    <div className="col-md-4">
                      <div className="item">
                        <label>Doanh số max</label>
                        <input
                          type="number"
                          className="form-control"
                          placeholder="Nhập vào thông tin"
                          value={option?.doanhSoSanPhamMax}
                          onChange={(e) => {
                            setOption({
                              ...option,
                              doanhSoSanPhamMax: e.target.value,
                            });
                          }}
                        />
                      </div>
                    </div>
                    <div className="col-md-4">
                      <div className="item">
                        <label>Tỷ lệ theo doanh số</label>
                        <input
                          type="number"
                          className="form-control"
                          placeholder="Nhập vào thông tin"
                          value={option?.tyLeTheoDoanhSo}
                          onChange={(e) => {
                            setOption({
                              ...option,
                              tyLeTheoDoanhSo: e.target.value,
                            });
                          }}
                        />
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-xl-12 col-md-12">
                      <div className="d-flex align-items-end justify-content-end h-100">
                        <div className="item">
                          <button
                            className="btn btn-success"
                            type="button"
                            data-dismiss="modal"
                            onClick={() => {
                              if (
                                option?.doanhSoSanPhamMin &&
                                option?.tyLeTheoDoanhSo
                              ) {
                                if (update.doanhSo) {
                                  let arrDoanhSo = [
                                    ...productInfo.doanhSoSanPham,
                                  ];
                                  const indexUpdate = arrDoanhSo
                                    .map((e) => e.id)
                                    .indexOf(update.idDoanhSo);
                                  if (indexUpdate !== -1) {
                                    arrDoanhSo[indexUpdate] = {
                                      ...arrDoanhSo[indexUpdate],
                                      start: option.doanhSoSanPhamMin,
                                      end: option.doanhSoSanPhamMax,
                                      tyLeTheoDoanhSo: option.tyLeTheoDoanhSo,
                                    };
                                    setProductInfo({
                                      ...productInfo,
                                      doanhSoSanPham: arrDoanhSo,
                                    });
                                    setUpdate({
                                      ...update,
                                      doanhSo: false,
                                      idDoanhSo: "",
                                    });
                                  }
                                } else {
                                  setProductInfo({
                                    ...productInfo,
                                    doanhSoSanPham: [
                                      ...productInfo.doanhSoSanPham,
                                      {
                                        id:
                                          productInfo.doanhSoSanPham.length + 1,
                                        start: option.doanhSoSanPhamMin,
                                        end: option.doanhSoSanPhamMax,
                                        tyLeTheoDoanhSo: option.tyLeTheoDoanhSo,
                                      },
                                    ],
                                  });
                                }
                                setOption({
                                  ...option,
                                  doanhSoSanPhamMin: "",
                                  doanhSoSanPhamMax: "",
                                  tyLeTheoDoanhSo: "",
                                });
                              }
                            }}
                          >
                            {update?.doanhSo ? "Cập nhật" : "Thêm"}
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-xl-12 col-md-12">
                      <div className="table-responsive">
                        <table
                          id="dtBasicExample"
                          className="table table-striped table-bordered table-sm"
                          cellSpacing="0"
                        >
                          <thead>
                            <tr>
                              <th className="th-sm text-center">
                                Doanh số min
                              </th>
                              <th className="th-sm text-center">
                                Doanh số max
                              </th>
                              <th className="th-sm text-center">
                                Tỷ lệ theo doanh số
                              </th>
                              <th className="th-sm text-center">Thao tác</th>
                            </tr>
                          </thead>
                          <tbody>
                            {productInfo.doanhSoSanPham.map((ds) => (
                              <tr>
                                <td>{ds.start}</td>
                                <td className="text-center">{ds.end}</td>
                                <td>{ds.tyLeTheoDoanhSo}</td>
                                <td className="text-center">
                                  <a
                                    style={{ cursor: "pointer" }}
                                    onClick={() => {
                                      setProductInfo({
                                        ...productInfo,
                                        doanhSoSanPham: [
                                          ...productInfo.doanhSoSanPham.filter(
                                            (e) => e.id !== ds.id
                                          ),
                                        ],
                                      });
                                    }}
                                  >
                                    Xóa
                                  </a>{" "}
                                  |{" "}
                                  <a
                                    style={{ cursor: "pointer" }}
                                    onClick={() => {
                                      setOption({
                                        ...option,
                                        doanhSoSanPhamMin: ds.start,
                                        doanhSoSanPhamMax: ds.end,
                                        tyLeTheoDoanhSo: ds.tyLeTheoDoanhSo,
                                      });
                                      setUpdate({
                                        ...update,
                                        doanhSo: true,
                                        idDoanhSo: ds.id,
                                      });
                                    }}
                                  >
                                    Sửa
                                  </a>
                                </td>
                              </tr>
                            ))}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-6">
                  <div className="item">
                    <label>Lãi suất</label>
                    <input
                      type="number"
                      className="form-control"
                      placeholder="Nhập vào lãi suất"
                      value={productInfo.laiSuat}
                      onChange={(e) => {
                        setProductInfo({
                          ...productInfo,
                          laiSuat: e.target.value,
                        });
                      }}
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="item">
                    <label>Tỷ lệ điểm theo lãi suất</label>
                    <input
                      type="number"
                      className="form-control"
                      placeholder="Nhập vào thông tin"
                      value={productInfo.tyLeTheoLaiSuat.congLaiSuat}
                      onChange={(e) => {
                        setProductInfo({
                          ...productInfo,
                          tyLeTheoLaiSuat: {
                            ...productInfo.tyLeTheoLaiSuat,
                            congLaiSuat: e.target.value,
                          },
                        });
                      }}
                    />
                  </div>
                  <div className="item">
                    <label>Tỷ lệ điểm không theo lãi suất</label>
                    <input
                      type="number"
                      className="form-control"
                      placeholder="Nhập vào thông tin"
                      value={productInfo.tyLeTheoLaiSuat.khongCongLaiSuat}
                      onChange={(e) => {
                        setProductInfo({
                          ...productInfo,
                          tyLeTheoLaiSuat: {
                            ...productInfo.tyLeTheoLaiSuat,
                            khongCongLaiSuat: e.target.value,
                          },
                        });
                      }}
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="item">
                    <label>Kỳ hạn</label>
                    <input
                      type="number"
                      className="form-control"
                      placeholder="Nhập vào thông tin"
                      value={productInfo.kyHan}
                      min={0}
                      max={12}
                      onChange={(e) => {
                        setProductInfo({
                          ...productInfo,
                          kyHan: e.target.value,
                        });
                      }}
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="item">
                    <label>Biên lợi nhuận</label>
                    <input
                      type="number"
                      className="form-control"
                      placeholder="Nhập vào thông tin"
                      value={productInfo.bienLoiNhuan}
                      onChange={(e) => {
                        setProductInfo({
                          ...productInfo,
                          bienLoiNhuan: e.target.value,
                        });
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="modal-footer">
              <button
                className="btn btn-success"
                type="button"
                data-dismiss="modal"
                onClick={async () => {
                  let newProduct = Object.assign({}, productInfo);
                  if (productInfo.categoryId.length < 1) {
                    newProduct.categoryId = categories[0].id;
                  }
                  try {
                    if (update.product) {
                      const updateProduct = await productApi.Update(
                        productInfo.id,
                        productInfo
                      );
                      if (updateProduct) {
                        setUpdate({ ...update, product: false });
                        const cloneProducts = [...dataSet];
                        const index = dataSet
                          .map((e) => e.id)
                          .indexOf(productInfo.id);
                        if (index !== -1) {
                          cloneProducts[index] = Object.assign({}, productInfo);
                          setDateSet(cloneProducts);
                        }
                      }
                    } else {
                      const createProduct = await productApi.Create(newProduct);
                      if (createProduct) {
                        setDateSet([
                          ...dataSet,
                          {
                            ...createProduct.data.data,
                            id: createProduct.data.data._id,
                          },
                        ]);
                      }
                    }
                  } catch (error) {
                  } finally {
                    setShowModal({ ...showModal, addProduct: false });
                    setProductInfo({
                      name: "",
                      maSanPham: "",
                      tyLeDiem: "",
                      soLuongSuDung: {
                        selectionType: 1,
                        soLuong: [],
                        tyLeTheoSoLuong: "",
                      },
                      doanhSoSanPham: [],
                      laiSuat: "",
                      tyLeTheoLaiSuat: {
                        congLaiSuat: "",
                        khongCongLaiSuat: "",
                      },
                      categoryId: "",
                      kyHan: "",
                      bienLoiNhuan: "",
                    });
                  }
                }}
              >
                {update.product ? "Cập nhật" : "Thêm"}
              </button>
              <button
                className="btn btn-secondary"
                type="button"
                data-dismiss="modal"
                aria-label="Close"
                onClick={() => {
                  setShowModal({ ...showModal, addProduct: false });
                  setUpdate({ ...update, product: false });
                  setProductInfo({
                    name: "",
                    maSanPham: "",
                    tyLeDiem: "",
                    soLuongSuDung: {
                      selectionType: 1,
                      soLuong: [],
                      tyLeTheoSoLuong: "",
                    },
                    doanhSoSanPham: [],
                    laiSuat: "",
                    tyLeTheoLaiSuat: {
                      congLaiSuat: "",
                      khongCongLaiSuat: "",
                    },
                    categoryId: "",
                    kyHan: "",
                    bienLoiNhuan: "",
                  });
                }}
              >
                Thoát
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

Tables.propTypes = {};

export default Tables;
