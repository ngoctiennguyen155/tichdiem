const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const products = new Schema({
  name: {
    type: String,
  },
  maSanPham: {
    type: String,
  },
  tyLeDiem: {
    type: Number,
  },
  soLuongSuDung: {
    selectionType: { type: Number },
    soLuong: [
      {
        start: { type: Number },
        end: { type: Number },
        tyLeTheoSoLuong: {
          type: Number,
        },
      },
    ],
    tyLeTheoSoLuong: {
      type: Number,
    },
  },
  doanhSoSanPham: [
    {
      start: { type: Number },
      end: { type: Number },
      tyLeTheoDoanhSo: { type: Number },
    },
  ],
  laiSuat: {
    type: Number,
  },
  tyLeTheoLaiSuat: {
    congLaiSuat: { type: Number },
    khongCongLaiSuat: { type: Number },
  },
  categoryId: {
    type: Schema.Types.ObjectId,
    ref: "categories",
  },
  kyHan: {
    type: Number,
  },
  bienLoiNhuan: {
    type: Number,
  },
});

module.exports = mongoose.model("products", products);
