const product = require("./product.model");
const user = require("./user.model");
const history = require("./histiry.model");
const category = require("./category.model");
const channel = require("./channel.model");
const buying = require("./buying.model");
module.exports = { product, user, history, category, channel, buying };
