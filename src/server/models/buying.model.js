const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const buyings = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: "users",
  },
  product: {
    type: String,
  },
  count: { type: Number },
});

module.exports = mongoose.model("buyings", buyings);
