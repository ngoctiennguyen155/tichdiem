const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const channels = new Schema({
  name: { type: String },
  maChannel: { type: String },
  tyLeDoiDiem: { type: Number },
});

module.exports = mongoose.model("channels", channels);
