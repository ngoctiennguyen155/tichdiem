const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const users = new Schema({
  name: { type: String },
  pointsUseable: { type: Number },
  pointsUsed: { type: Number },
  totalPoints: { type: Number },
  cif: { type: String },
  histories: [{ type: Schema.Types.ObjectId, ref: "histories" }],
});

module.exports = mongoose.model("users", users);
