const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const histories = new Schema({
  user: {
    type: mongoose.Types.ObjectId,
    ref: "users",
  },
  product: {
    type: Schema.Types.ObjectId,
    ref: "products",
  },
  channelId: [
    {
      type: Schema.Types.ObjectId,
      ref: "channels",
    },
  ],
  soDus: [
    {
      type: Number,
    },
  ],
  points: [{ type: Number }],
  dates: [{ type: Date }],
  count: { type: Number },
});

module.exports = mongoose.model("histories", histories);
