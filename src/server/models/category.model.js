const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const categories = new Schema({
  name: { type: String },
  tyLeDoiDiem: { type: Number },
  tyLeTrichChoKhachHang: { type: Number },
});

module.exports = mongoose.model("categories", categories);
