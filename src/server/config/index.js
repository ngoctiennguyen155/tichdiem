require("dotenv").config();
module.exports = {
  env: process.env.NODE_ENV || "development",
  development: {
    port: process.env.PORT || 8080,
    mongoUri:
      process.env.MONGODB_URI ||
      "mongodb://tien:tien@localhost:27017/tichdiem?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false",
  },
  production: {},
};
