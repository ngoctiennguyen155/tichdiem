module.exports = {
  customPromise: (callback) => {
    return new Promise((resolve, reject) => {
      callback()
        .then((result) => {
          resolve(result ?? true);
        })
        .catch((err) => reject(err));
    });
  },
};
