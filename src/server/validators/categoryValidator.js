const joi = require("joi");

module.exports = {
  createValidator: joi.object({
    name: joi.string().required(),
    tyLeDoiDiem: joi.number().required(),
    tyLeTrichChoKhachHang: joi.number().required(),
  }),
  deleteValidator: joi.object({
    id: joi.string().required(),
  }),
  paginationValidator: joi.object({
    page: joi.number().default(1),
    limit: joi.number().default(10),
  }),
  updateValidator: joi.object({
    name: joi.string().required(),
    tyLeDoiDiem: joi.number().required(),
    tyLeTrichChoKhachHang: joi.number().required(),
  }),
};
