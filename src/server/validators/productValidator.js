const joi = require("joi");

module.exports = {
  createValidator: joi.object({
    name: joi.string().required(),
    maSanPham: joi.string().required(),
    tyLeDiem: joi.number().required(),
    soLuongSuDung: joi
      .object({
        selectionType: joi.number().required(),
        soLuong: joi.array().items(
          joi.object({
            id: joi.any(),
            start: joi.number().required(),
            end: joi.number().required(),
            tyLeTheoSoLuong: joi.any().required(), // number
          })
        ),
        tyLeTheoSoLuong: joi.any().required(), // number
      })
      .required(),
    doanhSoSanPham: joi
      .array()
      .items(
        joi.object({
          id: joi.any(),
          start: joi.number().required(),
          end: joi.number().required(),
          tyLeTheoDoanhSo: joi.number().required(),
        })
      )
      .required(),
    laiSuat: joi.number().required(),
    tyLeTheoLaiSuat: joi
      .object({
        congLaiSuat: joi.number().required(),
        khongCongLaiSuat: joi.number().required(),
      })
      .required(),
    categoryId: joi.string().required(),
    kyHan: joi.number().required(),
    bienLoiNhuan: joi.number().required(),
  }),
  paginationValidator: joi.object({
    page: joi.number().default(1),
    limit: joi.number().default(10),
  }),
  updateValidator: joi.object({
    id: joi.any(),
    _id: joi.any(),
    __v: joi.any(),
    name: joi.string().required(),
    maSanPham: joi.string().required(),
    tyLeDiem: joi.number().required(),
    soLuongSuDung: joi
      .object({
        selectionType: joi.number().required(),
        soLuong: joi.array().items(
          joi.object({
            id: joi.any(),
            _id: joi.any(),
            start: joi.number().required(),
            end: joi.number().required(),
            tyLeTheoSoLuong: joi.any().required(), // number
          })
        ),
        tyLeTheoSoLuong: joi.any().required(), // number
      })
      .required(),
    doanhSoSanPham: joi
      .array()
      .items(
        joi.object({
          id: joi.any(),
          _id: joi.any(),
          start: joi.number().required(),
          end: joi.number().required(),
          tyLeTheoDoanhSo: joi.number().required(),
        })
      )
      .required(),
    laiSuat: joi.number().required(),
    tyLeTheoLaiSuat: joi
      .object({
        congLaiSuat: joi.number().required(),
        khongCongLaiSuat: joi.number().required(),
      })
      .required(),
    categoryId: joi.string().required(),
    kyHan: joi.number().required(),
    bienLoiNhuan: joi.number().required(),
  }),
};
