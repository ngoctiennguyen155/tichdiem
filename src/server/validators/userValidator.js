const joi = require("joi");

module.exports = {
  createValidator: joi.object({
    name: joi.string().required(),
    cif: joi.string().required(),
    pointsUseable: joi.number().required(),
    pointsUsed: joi.number().required(),
    totalPoints: joi.number().required(),
  }),
  paginationValidator: joi.object({
    page: joi.number().default(1),
    limit: joi.number().default(10),
  }),
};
