const ApiError = require("../helpers/errorHandle");
module.exports = {
  errorHandler: (err, req, res, next) => {
    console.log({
      route: req.url,
      method: req.method,
      error: err,
      date: new Date().toLocaleString(),
    });
    if (err instanceof ApiError) {
      return res.status(err.code).json(err);
    }
    return res.status(500).json({ message: "Internal Server Error" });
  },
};
