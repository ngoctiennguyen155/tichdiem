const mongoose = require("mongoose");
const config = require("../config");

module.exports = {
  mongoConnection: async (uri) => {
    console.log({ MONGODB_URI: uri });
    await mongoose.connect(uri);
    console.log(`connect to ${uri} success`);
  },
};
