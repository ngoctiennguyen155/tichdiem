const service = require("../services");
const ApiError = require("../helpers/errorHandle");
const {
  createValidator,
  updateValidator,
  paginationValidator,
} = require("../validators/productValidator");
module.exports = {
  getAll: async (req, res, next) => {
    try {
      const { value, error } = paginationValidator.validate(req.query);
      if (error) throw ApiError.badRequest(error);
      const data = await service.MG_GetAll("product", req.query, value);
      if (data) {
        return res.status(200).json({ message: "success", ...data });
      }
    } catch (error) {
      return next(error);
    }
  },
  create: async (req, res, next) => {
    try {
      const { value, error } = createValidator.validate(req.body);
      if (error) {
        throw ApiError.badRequest(error);
      }
      const result = await service.MG_Create("product", value);
      if (result) {
        return res.status(201).json({ message: "created", data: result });
      }
    } catch (error) {
      return next(error);
    }
  },
  delete: async (req, res, next) => {
    try {
      const { id } = req.params;
      const response = await service.MG_Delete("product", id);
      return res.status(200).json({ message: "success", data: response?.body });
    } catch (error) {
      return next(error);
    }
  },
  update: async (req, res, next) => {
    try {
      const { id } = req.params;
      const { value, error } = updateValidator.validate(req.body);
      if (error) {
        throw ApiError.badRequest(error);
      }
      const response = await service.MG_UpdateById("product", id, value);
      return res.status(200).json({ message: "success", data: response });
    } catch (error) {
      return next(error);
    }
  },
};
