const service = require("../services");

const {
  createValidator,
  deleteValidator,
  updateValidator,
  paginationValidator,
} = require("../validators/channelValidator");

const ApiError = require("../helpers/errorHandle");

module.exports = {
  getAll: async (req, res, next) => {
    try {
      const { value, error } = paginationValidator.validate(req.query);
      if (error) {
        throw ApiError.badRequest(error);
      }
      const data = await service.MG_GetAll("channel", req.query, value);
      if (data) {
        return res.status(200).json({ message: "success", ...data });
      }
    } catch (error) {
      return next(error);
    }
  },
  create: async (req, res, next) => {
    try {
      const { value, error } = createValidator.validate(req.body);
      if (error) {
        throw ApiError.badRequest(error);
      }
      const result = await service.MG_Create("channel", value);
      if (result) {
        return res.status(201).json({ message: "created", data: result });
      }
    } catch (error) {
      return next(error);
    }
  },
  delete: async (req, res, next) => {
    try {
      const { value, error } = deleteValidator.validate(req.params);
      if (error) throw ApiError.badRequest(error);
      const response = await service.MG_Delete("channel", value.id);
      return res.status(200).json({ message: "success", data: response });
    } catch (error) {
      return next(error);
    }
  },
  update: async (req, res, next) => {
    try {
      const { value, error } = deleteValidator.validate(req.params);
      if (error) throw ApiError.badRequest(error);
      const { value: value2, error: error2 } = updateValidator.validate(
        req.body
      );
      if (error) throw ApiError.badRequest(error2);
      const response = await service.MG_UpdateById("channel", value.id, value2);
      return res.status(200).json({ message: "success", data: response });
    } catch (error) {
      return next(error);
    }
  },
};
