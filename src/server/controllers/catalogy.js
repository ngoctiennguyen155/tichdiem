const service = require("../services");

const {
  createValidator,
  updateValidator,
  deleteValidator,
  paginationValidator,
} = require("../validators/categoryValidator");

const ApiError = require("../helpers/errorHandle");

module.exports = {
  getAll: async (req, res, next) => {
    try {
      const { value, error } = paginationValidator.validate(req.query);
      if (error) throw ApiError.badRequest(error);
      const data = await service.MG_GetAll("category", req.query, value);
      if (data) {
        return res.status(200).json({ message: "success", ...data });
      }
    } catch (error) {
      return next(error);
    }
  },
  create: async (req, res, next) => {
    try {
      const { value, error } = createValidator.validate(req.body);
      if (error) {
        throw ApiError.badRequest(error);
      }
      const result = await service.MG_Create("category", value);
      if (result) {
        return res.status(201).json({ message: "created", data: result });
      }
    } catch (error) {
      return next(error);
    }
  },
  delete: async (req, res, next) => {
    try {
      const { value, error } = deleteValidator.validate(req.params);
      if (error) throw ApiError.badRequest(error);
      const checkExistProuct = await service.MG_GetOne("product", {
        categoryId: value.id,
      });
      if (checkExistProuct)
        throw ApiError.badRequest({
          message: "Không thể xóa danh mục đang có sản phẩm",
        });
      const response = await service.MG_Delete("category", value.id);
      return res.status(200).json({ message: "success", data: response });
    } catch (error) {
      return next(error);
    }
  },
  update: async (req, res, next) => {
    try {
      const { id } = req.params;
      const { value, error } = updateValidator.validate(req.body);
      if (error) throw ApiError.badRequest(error);
      const response = await service.MG_UpdateById("category", id, value);
      return res.status(200).json({ message: "success", data: response });
    } catch (error) {
      return next(error);
    }
  },
};
