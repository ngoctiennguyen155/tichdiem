const service = require("../services");
const {
  createValidator,
  paginationValidator,
} = require("../validators/userValidator");
const ApiError = require("../helpers/errorHandle");
module.exports = {
  getById: async (req, res, next) => {
    try {
      const { id } = req.params;
      const data = await service.MG_GetById("user", id);
      if (data) {
        return res.status(200).json({ message: "success", data });
      }
    } catch (error) {
      return next(error);
    }
  },
  getAll: async (req, res, next) => {
    try {
      const query = req.query;
      const { value, error } = paginationValidator.validate(req.query);
      const data = await service.MG_GetAll("user", query, value);
      if (data) {
        return res.status(200).json({ message: "success", ...data });
      }
    } catch (error) {
      return next(error);
    }
  },
  create: async (req, res, next) => {
    try {
      const { value, error } = createValidator.validate(req.body);
      if (error) {
        throw ApiError.badRequest(error);
      }
      const result = await service.MG_Create("user", value);
      if (result) {
        return res.status(201).json({ message: "created", data: result });
      }
    } catch (error) {
      return next(error);
    }
  },
  delete: async (req, res, next) => {
    try {
      const { id } = req.params;
      const response = await service.MG_Delete("user", id);
      return res.status(200).json({ message: "success", data: response });
    } catch (error) {
      return next(error);
    }
  },
  update: async (req, res, next) => {
    try {
      const { id } = req.params;
      const { value, error } = createValidator.validate(req.body);
      if (error) throw ApiError.badRequest(error);
      const response = await updateUser("user", id, value);
      return res.status(200).json({ message: "success", data: response });
    } catch (error) {
      return next();
    }
  },
};
