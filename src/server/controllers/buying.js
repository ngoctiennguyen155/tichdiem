const userModel = require("../models/user.model");
const {
  getAll: getBuyings,
  getById: getBuyingsId,
} = require("../services/get");
const { create: createBuying } = require("../services/post");
const formatError = require("../utils/formatError");
module.exports = {
  getAll: async (req, res) => {
    try {
      const query = req.query;
      const data = await getBuyings("buying", query);
      if (data) {
        return res.status(200).json({ message: "success", data });
      }
    } catch (error) {
      console.log({ message: "error getall buying", error });
      return res.status(500).json({ message: "failed" });
    }
  },
  create: async (req, res) => {
    try {
      const { user, product, count } = req.body;
      if (!user || !product || !count) {
        return res.status(400).json({ message: "missing data" });
      }
      const checkUserExist = await userModel.findOne({ cif: user });
      if (checkUserExist) {
        const condition = await userModel.findOneAndUpdate(
          { cif: user, pointsUseable: { $gte: +count } },
          {
            $inc: {
              pointsUsed: +count,
              pointsUseable: -(count - 0),
            },
          }
        );
        if (condition) {
          const result = await createBuying("buying", {
            ...Object.assign(req.body, { user: checkUserExist._id }),
          });
          if (result) {
            return res.status(201).json({ message: "created", data: result });
          } else throw "created failed";
        } else throw "not enough points";
      } else
        return res
          .status(201)
          .json({ message: "failed", message: "user not found" });
    } catch (error) {
      console.log({ message: "error create buying", error });
      return res.status(400).json({ message: "failed" });
    }
  },
  delete: async (req, res) => {
    try {
      const { id } = req.params;
      if (!id) return res.status(400).json({ message: "missing data" });
      const response = await deleteCategory("category", id);
      return res.status(200).json({ message: "success", data: response });
    } catch (error) {
      console.log({ message: "error delete buying", error });
      return res.status(500).json({ message: "failed" });
    }
  },
  update: async (req, res) => {
    try {
      const { id } = req.params;
      const { name, tyLeDoiDiem, tyLeTrichChoKhachHang } = req.body;
      if (!id || !name || !tyLeDoiDiem || !tyLeTrichChoKhachHang)
        return res.status(400).json({ message: "missing data" });
      const response = await updateCategory("category", id, {
        name,
        tyLeDoiDiem,
        tyLeTrichChoKhachHang,
      });
      return res.status(200).json({ message: "success", data: response });
    } catch (error) {
      console.log({ message: "error update buying", error });
      return res.status(500).json({ message: "failed" });
    }
  },
};
