const categoryModel = require("../models/category.model");
const channelModel = require("../models/channel.model");
const histiryModel = require("../models/histiry.model");
const productModel = require("../models/product.model");
const userModel = require("../models/user.model");
const { getAll, getById, getOne } = require("../services/get");
const { create } = require("../services/post");
const { updateInc } = require("../services/update");
const formatError = require("../utils/formatError");
module.exports = {
  get: async (req, res) => {
    try {
      const { id } = req.params;
      const data = await getById("history", id);
      if (data) {
        return res.status(200).json({ message: "success", data });
      }
    } catch (error) {
      return res
        .status(404)
        .json({ message: "not found", data: formatError(error) });
    }
  },
  getAll: async (req, res) => {
    try {
      const query = req.query;
      const data = await histiryModel
        .find(query)
        .populate("product")
        .populate("channelId");
      if (data) {
        return res.status(200).json({ message: "success", data });
      }
    } catch (error) {
      return res
        .status(404)
        .json({ message: "not found", data: formatError(error) });
    }
  },
  getOne: async (req, res) => {
    try {
      const query = req.query;
      const data = await getOne("history", query);
      if (data) {
        return res.status(200).json({ message: "success", data });
      }
    } catch (error) {
      return res
        .status(404)
        .json({ message: "not found", data: formatError(error) });
    }
  },
  create: async (req, res) => {
    try {
      const { user, product, channelId, soDus, selectLaiSuat } = req.body;
      if (!user || !product) {
        return res.status(400).json({ message: "missing data" });
      }

      const checkUser = await userModel.findById(user);
      const checkProduct = await productModel.findById(product);
      if (!checkUser || !checkProduct) {
        return res.status(400).json({ message: "data not found" });
      }

      let checkExist = await getOne("history", { user, product });
      if (checkExist) {
        checkExist = await updateInc(
          "history",
          checkExist._id,
          { count: +1 },
          { soDus: soDus - 0, channelId: channelId, dates: Date.now() }
        );
      } else {
        checkExist = await create("history", {
          ...req.body,
          dates: Date.now(),
          count: 1,
        });
      }
      if (checkExist) {
        // tinh diem + user
        const checkChannel = await channelModel.findById(channelId);
        const checkCategory = await categoryModel.findById(
          checkProduct.categoryId
        );
        const nSLProduct = await histiryModel.findOne({ user, product });
        const tyLeTheoSoLuong =
          checkProduct?.soLuongSuDung?.selectionType === 2
            ? checkProduct?.soLuongSuDung?.tyLeTheoSoLuong
            : checkProduct?.soLuongSuDung?.soLuong
                ?.filter((sl) => nSLProduct.count >= sl.start)
                ?.pop()?.tyLeTheoSoLuong;
        if (checkUser && checkProduct && checkChannel && checkCategory) {
          const result =
            ((((soDus - 0) *
              (checkProduct?.kyHan - 0) *
              (checkProduct?.bienLoiNhuan - 0)) /
              100) *
              (checkCategory?.tyLeTrichChoKhachHang - 0)) /
            100 /
            12;
          const nChannel = checkChannel?.tyLeDoiDiem;
          const nCategory = checkCategory?.tyLeDoiDiem;
          const nProduct = checkProduct?.tyLeDiem;
          const nDoanhSoProduct = checkProduct?.doanhSoSanPham
            ?.filter((tyle) => soDus >= tyle.start)
            ?.pop()?.tyLeTheoDoanhSo;

          const nLaiSuatProduct =
            selectLaiSuat === "1"
              ? checkProduct?.tyLeTheoLaiSuat?.congLaiSuat
              : checkProduct?.tyLeTheoLaiSuat?.khongCongLaiSuat;
          let resPoint =
            (nLaiSuatProduct *
              nDoanhSoProduct *
              nSLProduct?.count *
              tyLeTheoSoLuong *
              nProduct *
              nCategory *
              nChannel *
              result) /
            1;
          if (!Number.isNaN(resPoint)) {
            resPoint = Math.round(resPoint);
            await userModel.findByIdAndUpdate(user, {
              $inc: { totalPoints: resPoint, pointsUseable: resPoint },
            });
            await updateInc(
              "history",
              checkExist._id,
              {},
              { points: resPoint }
            );
            return res.status(201).json({ message: "created", data: resPoint });
          }
        }
        //
        return res.status(201).json({ message: "failed" });
      }
    } catch (error) {
      console.log({ postChannelError: error });
      return res.status(400).json({ message: "failed" });
    }
  },
};
