const { MG_Delete } = require("./delete");
const { MG_GetAll, MG_GetById, MG_GetOne } = require("./get");
const { MG_Create } = require("./post");
const { MG_UpdateById, MG_UpdateOne } = require("./update");
module.exports = {
  MG_Delete,
  MG_GetAll,
  MG_GetById,
  MG_GetOne,
  MG_Create,
  MG_UpdateById,
  MG_UpdateOne,
};
