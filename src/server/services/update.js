const client = require("../libs/elasticsearch");
const models = require("../models");
module.exports = {
  MG_UpdateById: (
    dbName,
    id,
    objectNew = {},
    objectInc = {},
    objectPush = {}
  ) => {
    return new Promise((resolve, reject) => {
      models[dbName].findByIdAndUpdate(
        id,
        { $push: objectPush, $inc: objectInc, $set: objectNew },
        { new: true },
        (err, result) => {
          if (err) return reject(err);
          return resolve(result);
        }
      );
    });
  },
  MG_UpdateOne: (
    dbName,
    query,
    objectNew = {},
    objectInc = {},
    objectPush = {}
  ) => {
    return new Promise((resolve, reject) => {
      models[dbName].findByIdAndUpdate(
        query,
        { $push: objectPush, $inc: objectInc, $set: objectNew },
        { new: true },
        (err, result) => {
          if (err) return reject(err);
          return resolve(result);
        }
      );
    });
  },
};
