const client = require("../libs/elasticsearch");
const models = require("../models");
module.exports = {
  MG_Create: (dbName, data) => {
    return new Promise((res, rej) => {
      models[dbName].create(data, (err, result) => {
        if (err) return rej(err);
        res(result);
      });
    });
  },
};
