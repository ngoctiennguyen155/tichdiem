const client = require("../libs/elasticsearch");
const models = require("../models");
module.exports = {
  MG_GetById: (dbName, id) => {
    return new Promise((resolve, reject) => {
      models[dbName]
        .findById(id)
        .then((result) => resolve(result))
        .catch((err) => reject(err));
    });
  },
  MG_GetAll: async (dbName, query = {}, pagination = null) => {
    if (pagination) {
      const total = await models[dbName].countDocuments(query);
      return new Promise((resolve, reject) => {
        models[dbName]
          .find(query)
          .skip((pagination.page - 1) * pagination.limit)
          .limit(pagination.limit)
          .then((result) => resolve({ data: result, total }))
          .catch((err) => reject(err));
      });
    }
    return new Promise((resolve, reject) => {
      models[dbName]
        .find(query)
        .then((result) => resolve({ data: result }))
        .catch((err) => reject(err));
    });
  },
  MG_GetOne: (dbName, query = {}) => {
    return new Promise((resolve, reject) => {
      models[dbName]
        .findOne(query)
        .then((result) => resolve(result))
        .catch((err) => reject(err));
    });
  },
};
