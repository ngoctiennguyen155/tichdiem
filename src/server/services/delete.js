const client = require("../libs/elasticsearch");
const models = require("../models");
module.exports = {
  MG_Delete: (dbName, id) => {
    return new Promise((resolve, reject) => {
      models[dbName].findByIdAndDelete(id, (err, result) => {
        if (err) return reject(err);
        return resolve(result);
      });
    });
  },
};
