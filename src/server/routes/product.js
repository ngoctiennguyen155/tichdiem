const productController = require("../controllers/product");
const router = require("express").Router();
const { tryCatch } = require("../helpers/tryCatch");

router.route("/products").get(tryCatch(productController.getAll));
router
  .route("/product/:id")
  .get(tryCatch(productController.get))
  .put(tryCatch(productController.update))
  .delete(tryCatch(productController.delete));
router.route("/product").post(tryCatch(productController.create));

module.exports = router;
