const historyController = require("../controllers/history");
const router = require("express").Router();
const { tryCatch } = require("../helpers/tryCatch");

router.route("/histories").get(tryCatch(historyController.getAll));
router.route("/history/:id").get(tryCatch(historyController.get));
router
  .route("/history")
  .post(tryCatch(historyController.create))
  .get(tryCatch(historyController.getOne));

module.exports = router;
