const buyingController = require("../controllers/buying");
const router = require("express").Router();
const { tryCatch } = require("../helpers/tryCatch");

router.route("/buyings").get(tryCatch(buyingController.getAll));
router.route("/buying").post(tryCatch(buyingController.create));

module.exports = router;
