const categoryController = require("../controllers/catalogy");
const router = require("express").Router();
const { tryCatch } = require("../helpers/tryCatch");

router.route("/categories").get(tryCatch(categoryController.getAll));
router
  .route("/category/:id")
  .get(tryCatch(categoryController.get))
  .put(tryCatch(categoryController.update))
  .delete(tryCatch(categoryController.delete));
router.route("/category").post(tryCatch(categoryController.create));

module.exports = router;
