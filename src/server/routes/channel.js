const channelController = require("../controllers/channel");
const router = require("express").Router();
const { tryCatch } = require("../helpers/tryCatch");

router.route("/channels").get(tryCatch(channelController.getAll));
router
  .route("/channel/:id")
  .get(tryCatch(channelController.getAll))
  .put(tryCatch(channelController.update))
  .delete(tryCatch(channelController.delete));
router.route("/channel").post(tryCatch(channelController.create));

module.exports = router;
