const userController = require("../controllers/user");
const router = require("express").Router();
const { tryCatch } = require("../helpers/tryCatch");

router.route("/users").get(tryCatch(userController.getAll));
router
  .route("/user/:id")
  .get(tryCatch(userController.getById))
  .put(tryCatch(userController.update))
  .delete(tryCatch(userController.delete));
router.route("/user").post(tryCatch(userController.create));

module.exports = router;
