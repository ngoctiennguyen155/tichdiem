const express = require("express");
const path = require("path");
const config = require("./config");
const app = express();
// const fs = require("fs");
// const { upload } = require("./helpers/uploadfile");
// const xlsx = require("node-xlsx");
// import custom
//// libs
const { mongoConnection } = require("./libs/mongodb");
const { errorHandler } = require("./middlewares/errorHandler");
try {
  mongoConnection(config[config.env].mongoUri);
  app.listen(config[config.env].port, () =>
    console.log(`Listening on port ${config[config.env].port}!`)
  );
} catch (error) {
  console.log({ error });
}

app.use(express.static(path.join(__dirname, "../../dist")));

app.use(express.json());

app.use(express.static("dist"));

app.use("/tichdiem/api", require("./routes/channel"));
app.use("/tichdiem/api", require("./routes/catelogy"));
app.use("/tichdiem/api", require("./routes/product"));
app.use("/tichdiem/api", require("./routes/user"));
app.use("/tichdiem/api", require("./routes/history"));
app.use("/tichdiem/api", require("./routes/buying"));

app.use(errorHandler);
// app.post("/tichdiem/api/uploadfile", async (req, res) => {
//   try {
//     upload(req, res, async function (err) {
//       if (err instanceof multer.MulterError) {
//         return res.status(500).json(err);
//       } else if (err) {
//         return res.status(500).json(err);
//       }
//       const workSheetsFromBuffer = xlsx.parse(
//         fs.readFileSync(`public/${req.file.filename}`)
//       );
//       const data = workSheetsFromBuffer[0].data.slice(1);
//       await Promise.all(
//         data.map(async (e) => {
//           const checkUser = await userModel.findOne({ cif: e[0] });
//           const checkProduct = await productModel.findOne({ maSanPham: e[1] });
//           const checkChannel = await channelModel.findOne({ maChannel: e[2] });
//           const checkCategory = await categoryModel.findById(
//             checkProduct?.categoryId
//           );
//           const checkLaiSuat = e[4] ? 1 : 2;
//           console.log({ checkLaiSuat, check: e[4] });
//           if (
//             checkUser &&
//             checkProduct &&
//             checkCategory &&
//             checkChannel &&
//             Number.isInteger(e[3])
//           ) {
//             let result;
//             const checkExist = await getOne("history", {
//               user: checkUser._id,
//               product: checkProduct._id,
//             });
//             if (checkExist) {
//               result = await updateInc(
//                 "history",
//                 checkExist._id,
//                 { count: +1 },
//                 {
//                   soDus: e[3] - 0,
//                   channelId: checkChannel._id,
//                   dates: Date.now(),
//                 }
//               );
//             } else {
//               result = await create("history", {
//                 user: checkUser._id,
//                 product: checkProduct._id,
//                 dates: Date.now(),
//                 soDus: e[3],
//                 count: 1,
//               });
//             }
//             if (result) {
//               // tinh diem + user
//               const nSLProduct = await histiryModel.findOne({
//                 user: checkUser._id,
//                 product: checkProduct._id,
//               });
//               const tyLeTheoSoLuong =
//                 checkProduct?.soLuongSuDung?.selectionType === 2
//                   ? checkProduct?.soLuongSuDung?.tyLeTheoSoLuong
//                   : checkProduct?.soLuongSuDung?.soLuong
//                       ?.filter((sl) => nSLProduct.count >= sl.start)
//                       ?.pop()?.tyLeTheoSoLuong;
//               const result =
//                 ((((e[3] - 0) *
//                   (checkProduct?.kyHan - 0) *
//                   (checkProduct?.bienLoiNhuan - 0)) /
//                   100) *
//                   (checkCategory?.tyLeTrichChoKhachHang - 0)) /
//                 100 /
//                 12;
//               const nChannel = checkChannel?.tyLeDoiDiem;
//               const nCategory = checkCategory?.tyLeDoiDiem;
//               const nProduct = checkProduct?.tyLeDiem;
//               const nDoanhSoProduct = checkProduct?.doanhSoSanPham
//                 ?.filter((tyle) => e[3] >= tyle.start)
//                 ?.pop()?.tyLeTheoDoanhSo;

//               const nLaiSuatProduct =
//                 checkLaiSuat === 1
//                   ? checkProduct?.tyLeTheoLaiSuat?.congLaiSuat
//                   : checkProduct?.tyLeTheoLaiSuat?.khongCongLaiSuat;
//               console.log(nLaiSuatProduct);
//               const resPoint =
//                 (nLaiSuatProduct *
//                   nDoanhSoProduct *
//                   nSLProduct?.count *
//                   tyLeTheoSoLuong *
//                   nProduct *
//                   nCategory *
//                   nChannel *
//                   result) /
//                 1;
//               if (!Number.isNaN(resPoint)) {
//                 await userModel.findByIdAndUpdate(checkUser._id, {
//                   $inc: { totalPoints: resPoint, pointsUseable: resPoint },
//                 });
//               }
//             }
//           }
//         })
//       );
//       return res.status(200).json({ message: "success" });
//     });
//   } catch (error) {
//     console.log({ message: "error upload file", error });
//     return res.status(500).json({ message: "failed" });
//   }
// });

app.get("/tichdiem/?*", function (req, res) {
  res.sendFile(path.join(__dirname, "../../dist", "index.html"));
});
