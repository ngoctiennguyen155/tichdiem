import React, { useState, useEffect } from "react";
import ContentWrapper from "../components/contentwrapper";
import SideBar from "../components/sidebar";
import productApi from "./api/product";
import "./assets/css/styles.css";
import "./assets/css/app.css";
import "./assets/css/admin.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
const App = () => {
  return (
    <>
      <Router basename="/tichdiem">
        <div id="wrapper">
          <SideBar />
          <ContentWrapper />
        </div>
        <a className="scroll-to-top rounded" href="#page-top">
          <i className="fas fa-angle-up"></i>
        </a>

        {/* <!-- Logout Modal--> */}
        <div
          className="modal fade"
          id="logoutModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  Bạn có muốn đăng xuất?
                </h5>
                <button
                  className="close"
                  type="button"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div className="modal-body">
                Bạn có chắc muốn đăng xuất khỏi hệ thống.
              </div>
              <div className="modal-footer">
                <button
                  className="btn btn-secondary"
                  type="button"
                  data-dismiss="modal"
                >
                  Hủy lệnh
                </button>
                <a className="btn btn-primary" href="login.html">
                  Đăng xuất
                </a>
              </div>
            </div>
          </div>
        </div>
      </Router>
    </>
  );
};

export default App;
