import axiosClient from "./axiosClient";
const productApi = {
  get: (params) => {
    const url = `/product/:id`;
    return axiosClient.get(url, { params });
  },
  getAll: (params) => {
    const url = `/products`;
    return axiosClient.get(url, { params });
  },
  Create: (data) => {
    const url = "/product";
    return axiosClient.post(url, data);
  },
  Update: (id, data) => {
    const url = `/product/${id}`;
    return axiosClient.put(url, data);
  },
  delete: (id) => {
    const url = `/product/${id}`;
    return axiosClient.delete(url);
  },
};

export default productApi;
