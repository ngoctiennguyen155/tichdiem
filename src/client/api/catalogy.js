import axiosClient from "./axiosClient";
const categoryApi = {
  get: (params) => {
    const url = `/category/:id`;
    return axiosClient.get(url, { params });
  },
  getAll: (params) => {
    const url = `/categories`;
    return axiosClient.get(url, { params });
  },
  Create: (data) => {
    const url = "/category";
    return axiosClient.post(url, data);
  },
  delete: (id) => {
    const url = `/category/${id}`;
    return axiosClient.delete(url);
  },
  Update: (id, data) => {
    const url = `/category/${id}`;
    return axiosClient.put(url, data);
  },
};

export default categoryApi;
