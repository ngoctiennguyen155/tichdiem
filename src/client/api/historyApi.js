import axiosClient from "./axiosClient";
const historyApi = {
  get: (params) => {
    const url = `/history/:id`;
    return axiosClient.get(url, { params });
  },
  getAll: (params) => {
    const url = `/histories`;
    return axiosClient.get(url, { params });
  },
  getOne: (params) => {
    const url = `/history`;
    return axiosClient.get(url, { params });
  },
  Create: (data) => {
    const url = "/history";
    return axiosClient.post(url, data);
  },
  Update: (id, data) => {
    const url = `/history/${id}`;
    return axiosClient.put(url, data);
  },
  delete: (id) => {
    const url = `/history/${id}`;
    return axiosClient.delete(url);
  },
};

export default historyApi;
