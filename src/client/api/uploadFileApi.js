import axiosClient from "./axiosClient";
const uploadFileApi = {
  Upload: (data) => {
    const url = "/uploadfile";
    return axiosClient.post(url, data);
  },
};

export default uploadFileApi;
