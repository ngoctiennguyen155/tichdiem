import axiosClient from "./axiosClient";
const channelApi = {
  get: (params) => {
    const url = `/channel/:id`;
    return axiosClient.get(url, { params });
  },
  getAll: (params) => {
    const url = `/channels`;
    return axiosClient.get(url, { params });
  },
  Create: (data) => {
    const url = "/channel";
    return axiosClient.post(url, data);
  },
  Update: (id, data) => {
    const url = `/channel/${id}`;
    return axiosClient.put(url, data);
  },
  delete: (id) => {
    const url = `/channel/${id}`;
    return axiosClient.delete(url);
  },
};

export default channelApi;
