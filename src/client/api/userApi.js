import axiosClient from "./axiosClient";
const userApi = {
  get: (params) => {
    const url = `/user/:id`;
    return axiosClient.get(url, { params });
  },
  getAll: (params) => {
    const url = `/users`;
    return axiosClient.get(url, { params });
  },
  Create: (data) => {
    const url = "/user";
    return axiosClient.post(url, data);
  },
  Update: (id, data) => {
    const url = `/user/${id}`;
    return axiosClient.put(url, data);
  },
  delete: (id) => {
    const url = `/user/${id}`;
    return axiosClient.delete(url);
  },
};

export default userApi;
